<?php
include("..\confidentials.php");//to be changed when site goes live
/*
########################################################
--------------------TESTS LIST--------------------------
########################################################
*/
echo "\nTESTING IF SERVER WORKS";
// url_test("http://localhost/"); //to be changed when site goes live
//on n'a pas d'implementation de BDD pour le moment :()
// echo "\nTESTING IF DATABASE CONNECTION WORKS";
// db_test(USERNAME, PASSWORD, "localhost", ""); //to be changed when site goes live
echo "\nTEST 1 - TESTING IF HOMEPAGE WORKS";
	echo "\nSTEP 1 - main page";
	url_test("http://localhost/nuit-de-l-info/canvas/index.html");//to be changed when site goes live
	echo "\nSTEP 2 - javascript";
	url_test("http://localhost/nuit-de-l-info/canvas/main.js");//to be changed when site goes live
echo "\nTEST 2 - TESTING IF HOMEPAGE IMAGES EXIST";
	echo "\nSTEP 1 - door-open-solid.png";
	url_test("http://localhost/nuit-de-l-info/canvas/img/door-open-solid.png");//to be changed when site goes live
	echo "\nSTEP 2 - exclamation-triangle-solid.png";
	url_test("http://localhost/nuit-de-l-info/canvas/img/exclamation-triangle-solid.png");//to be changed when site goes live
	echo "\nSTEP 3 - gamepad-solid.png";
	url_test("http://localhost/nuit-de-l-info/canvas/img/gamepad-solid.png");//to be changed when site goes live
	echo "\nSTEP 4 - heartbeat-solid.png";
	url_test("http://localhost/nuit-de-l-info/canvas/img/heartbeat-solid.png");//to be changed when site goes live
	echo "\nSTEP 5 - sun-solid.png";
	url_test("http://localhost/nuit-de-l-info/canvas/img/sun-solid.png");//to be changed when site goes live
	echo "\nSTEP 6 - thermometer-empty-solid.png";
	url_test("http://localhost/nuit-de-l-info/canvas/img/thermometer-empty-solid.png");//to be changed when site goes live
	echo "\nSTEP 7 - wind-solid.png";
	url_test("http://localhost/nuit-de-l-info/canvas/img/wind-solid.png");//to be changed when site goes live
echo "\nTEST 3 - TESTING IF HEART CHECKER EXISTS";
url_test("http://localhost/nuit-de-l-info/cardiaque/index.html");//to be changed when site goes live
echo "\nTEST 4 - TESTING IF CHECKLIST EXISTS";
	echo "\nSTEP 1 - main page";
	url_test("http://localhost/nuit-de-l-info/checklist/index.html");//to be changed when site goes live
	echo "\nSTEP 2 - css";
	url_test("http://localhost/nuit-de-l-info/checklist/main.css");//to be changed when site goes live
	echo "\nSTEP 3 - javascript";
	url_test("http://localhost/nuit-de-l-info/checklist/main.js");//to be changed when site goes live
	
echo "\nEND OF TESTS";
//GO TO NEW LINE AFTER TESTS
echo "
";



/*
########################################################
----------------GENERAL TEST FUNCTIONS------------------
########################################################
*/
//testing if a specific page responds// source:https://www.saotn.org/php-curl-check-website-availability/
function url_test($url){
	$result = "";
	$timeout = 10;
	$ch = curl_init();
	curl_setopt ( $ch, CURLOPT_URL, $url );
  curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
	curl_setopt ( $ch, CURLOPT_TIMEOUT, $timeout );
	$http_respond = curl_exec($ch);
	$http_respond = trim( strip_tags( $http_respond ) );
	$http_code = curl_getinfo( $ch, CURLINFO_HTTP_CODE );
	//if we don' get valid connection code (200 or 302)
	if ( ( $http_code !="200") && ( $http_code != "302" ) ) {
	 	//$result .= "\e[41;4;33m[\u@\h \W]$";
		$result .= "ERROR code:";
		$result .= $http_code;
		$result .= "\n";
		echo $result;
		die();
   	}
	curl_close( $ch );
}

function db_test($username, $password, $server, $dbname){
	$link = mysqli_connect('127.0.0.1', $username, $password, $dbname);
	$result ="";
	if (!$link) {
	    $result .= "Error: Unable to connect to MySQL." . PHP_EOL;
	    $result .= "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
	    $result .= "Debugging error: " . mysqli_connect_error() . PHP_EOL;
			echo $result;
	    exit;
	}
	mysqli_close($link);
}
?>
