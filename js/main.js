let canvas, ctx;
let objects = [];
let preload;
let images = {};
let sliding = false;
let count = 0;
let button;
let landscape;

const PRELOAD_IMAGES = {
	heartbeat: 1,
	warning: 2,
	door: 3,
	gamepad: 4,
	sun: 5,
	thermometer: 6,
	wind: 7
}
const CORNER = {
	TOP_LEFT: 1,
	TOP_RIGHT: 2,
	BOTTOM_RIGHT: 3,
	BOTTOM_LEFT: 4
}

window.addEventListener("load", setup, false);

Math.easeInCubic = function (t, b, c, d)
{
	t /= d;
	return c*t*t*t + b;
};
function setup()
{
	canvas = document.querySelector("canvas");
	ctx = canvas.getContext("2d");

	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;

	landscape = canvas.width > canvas.height;

	window.addEventListener("resize", function() {
		canvas.width = window.innerWidth;
		canvas.height = window.innerHeight;

		landscape = canvas.width > canvas.height;
	});

	window.addEventListener("mouseup", function(e) {
		if(alert.click(e.pageX, e.pageY))
			return alert.close();
		if(button.click(e.pageX, e.pageY))
			return location.href = button.link;
		for(let i of objects)
			if(i.click(e.pageX, e.pageY))
				return sliding = i.link;
	});

	images[PRELOAD_IMAGES.heartbeat] = new Image();
	images[PRELOAD_IMAGES.heartbeat].src = "img/heartbeat-solid.png";
	images[PRELOAD_IMAGES.warning] = new Image();
	images[PRELOAD_IMAGES.warning].src = "img/exclamation-triangle-solid.png";
	images[PRELOAD_IMAGES.door] = new Image();
	images[PRELOAD_IMAGES.door].src = "img/door-open-solid.png";
	images[PRELOAD_IMAGES.gamepad] = new Image();
	images[PRELOAD_IMAGES.gamepad].src = "img/gamepad-solid.png";
	images[PRELOAD_IMAGES.sun] = new Image();
	images[PRELOAD_IMAGES.sun].src = "img/sun-solid.png";
	images[PRELOAD_IMAGES.thermometer] = new Image();
	images[PRELOAD_IMAGES.thermometer].src = "img/thermometer-empty-solid.png";
	images[PRELOAD_IMAGES.wind] = new Image();
	images[PRELOAD_IMAGES.wind].src = "img/wind-solid.png";

	setTimeout(function() {
		if(landscape)
		{
			objects.push(new Data(0, 0, images[PRELOAD_IMAGES.heartbeat], "100 bpm", "dashboard.html"));
			objects.push(new Data(canvas.width / 3, 0, images[PRELOAD_IMAGES.thermometer], "45 °C", "dashboard.html", images[PRELOAD_IMAGES.sun], CORNER.TOP_RIGHT));
			objects.push(new Data(2 * canvas.width / 3, 0, images[PRELOAD_IMAGES.wind], "26km/h", "dashboard.html"));
			objects.push(new Data(0, canvas.height / 2, images[PRELOAD_IMAGES.thermometer], "23 °C", "dashboard.html", images[PRELOAD_IMAGES.door], CORNER.TOP_RIGHT));
			objects.push(new Data(canvas.width / 3, canvas.height / 2, images[PRELOAD_IMAGES.gamepad], "", "#gamepad", images[PRELOAD_IMAGES.warning], CORNER.BOTTOM_RIGHT));
			objects.push(new Data(2 * canvas.width / 3, canvas.height / 2, images[PRELOAD_IMAGES.warning], "", "#warn"));
		}
		else
		{
			objects.push(new Data(0, 0, images[PRELOAD_IMAGES.heartbeat], "100 bpm", "dashboard.html"));
			objects.push(new Data(canvas.width / 2, 0, images[PRELOAD_IMAGES.thermometer], "45 °C", "dashboard.html", images[PRELOAD_IMAGES.sun], CORNER.TOP_RIGHT));
			objects.push(new Data(0, canvas.height / 4, images[PRELOAD_IMAGES.wind], "26km/h", "dashboard.html"));
			objects.push(new Data(canvas.width / 2, canvas.height / 4, images[PRELOAD_IMAGES.thermometer], "23 °C", "dashboard.html", images[PRELOAD_IMAGES.door], CORNER.TOP_RIGHT));
			objects.push(new Data(0, 2 * canvas.height / 4, images[PRELOAD_IMAGES.gamepad], "", "#gamepad", images[PRELOAD_IMAGES.warning], CORNER.BOTTOM_RIGHT));
			objects.push(new Data(canvas.width / 2, 2 * canvas.height / 4, images[PRELOAD_IMAGES.warning], "", "#warn"));
		}

		alert = new Alert("Attention");

		button = new Button(-1, canvas.height - 50, canvas.width + 1, 50, "checklist.html");

		drawCanvas();
	}, 1000);
}
function drawCanvas()
{
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	ctx.fillStyle = "#313131";
	ctx.fillRect(0, 0, canvas.width, canvas.height);
	ctx.font = "55px \"Segoe UI\"";
	ctx.fillStyle = "white";
	for(let i of objects)
		i.draw(ctx);

	if(alert)
		alert.draw(ctx);
	ctx.fillStyle = "white";
	if(button)
		button.draw(ctx);

	if(sliding)
	{
		for(let i of objects)
			i.move(Math.easeInCubic(count, 10, 40, 25), 0);
		count++;
		if(count == 30)
			location.href = sliding;
	}

	requestAnimationFrame(drawCanvas);
}
//Classe pour les informations disponible sur l'accueil
class Data
{
	//Image -> l'image affichée, data -> Le texte qui accompagne l'image, link -> Le lien. Tout simplement, secondary -> Seconde image plus petite, pos -> Position de la seconde image (dans un coin)
	constructor(x, y, image, data, link, secondary, pos)
	{
		if(image == undefined)
			return;
		this.x = x || 0;
		this.y = y || 0;
		this.image = image;
		this.data = data || "";
		this.secondary = secondary || null;
		this.pos = pos || CORNER.TOP_RIGHT;
		this.link = link;

		this.width = (this.image.width > this.image.height ? 240 : this.image.width / this.image.height * 240);
		this.height = (this.image.height > this.image.width ? 240 : this.image.height / this.image.width * 240);

		if(landscape)
		{
			this.fullWidth = canvas.width / 3;
			this.fullHeight = canvas.height / 2;
		}
		else
		{
			this.fullWidth = canvas.width / 2;
			this.fullHeight = canvas.height / 3;
		}
	}
	draw(ctx)
	{
		ctx.drawImage(this.image, this.x + this.fullWidth / 2 - this.width / 2, this.y + this.fullHeight / 2 - this.height, this.width, this.height);
		ctx.fillText(this.data, this.x + this.fullWidth / 2 - ctx.measureText(this.data).width / 2, this.y + this.fullHeight - this.height + 60);
		if(this.secondary)
		{
			if(this.pos == CORNER.TOP_LEFT)
				ctx.drawImage(this.secondary, this.x - this.fullWidth / 2 + 120, this.y, 80, 80);
			if(this.pos == CORNER.TOP_RIGHT)
				ctx.drawImage(this.secondary, this.fullWidth + this.x - this.fullWidth / 2 + 120, this.y, 80, 80);
			if(this.pos == CORNER.BOTTOM_RIGHT)
				ctx.drawImage(this.secondary, this.fullWidth + this.x - this.fullWidth / 2 + 120, this.fullHeight / 2  + this.y, 80, 80);
			if(this.pos == CORNER.BOTTOM_LEFT)
				ctx.drawImage(this.secondary, this.x - this.fullWidth / 2 + 120, this.fullHeight / 2 + this.y, 80, 80);
		}
	}
	click(x, y)
	{
		return this.x < x && this.x + this.fullWidth > x && this.y < y && this.y + this.fullHeight > y;
	}
	setSecondary(secondary, pos)
	{
		this.secondary = secondary;
		this.pos = pos;
	}
	setInfo(info)
	{
		this.data = info;
	}
	move(x, y)
	{
		this.x += x;
		this.y += y;
	}
}
class Alert
{
	constructor(message)
	{
		this.message = message;
		this.y = -80;
	}
	draw(ctx)
	{
		if(this.closing)
		{
			if(this.y > -80)
				this.y -= 80/15;
		}
		else
		{
			if(this.y < 0)
				this.y += 80/15;
			else
				this.y = 0;
		}
		if(this.y > -80)
		{
			ctx.fillStyle = "#c0392b";
			ctx.fillRect(0, this.y, canvas.width, 80);
			ctx.fillStyle = "white";
			ctx.fillText(this.message, canvas.width / 2 - ctx.measureText(this.message).width / 2, this.y + 55);
		}
	}
	click(x, y)
	{
		return 0 < x && canvas.width > x && this.y < y && this.y + 80 > y;
	}
	close()
	{
		this.closing = true;
	}
}
class Button
{
	constructor(x, y, width, height, link)
	{
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
		this.link = link;
	}
	draw(ctx)
	{
		ctx.fillRect(this.x, this.y, this.x + this.width, this.y + this.height);
	}
	click(x, y)
	{
		return this.x < x && this.x + this.width > x && this.y < y && this.y + this.height > y;
	}
}
