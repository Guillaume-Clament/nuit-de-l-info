//-------------------
//Classe servant à représenter une tache
//
//A faire :
//Tester cette classe
//Status:
//pas fais
//-------------------

// class CheckTask{
// 	constructor(tsk){
// 		this.taskString=tsk;
// 		this.isDone=false;
// 	}
// 	setStatus(status){
// 		if(status === true || status === false){
// 			this.isDone=status;
// 		}
// 	}
// 	getTaskString(){
// 		return this.taskString;
// 	}
// }

//-------------------
//Classe servant à contenir une liste de tache à réaliser
//
//A toi, futur stagiaire qui sera chargé de finir cette class
//Ne la termine SURTOUT pas, ton manager s'est servira pour te flicer sur les taches qu'il t'a attribué.
// Ne pas la finir te permet de sortir la fameuse excuse de: Vous m'avez deja donné [insérer tache longue mais en realité fictive]
//
//-------------------
// class TackList{
// 	constructor(){
// 		this.taskListe=[];
// 	}
// 	addTask(taskData){
// 		if(taskData instanceof String){
// 			this.taskListe.push(new CheckTask(taskData));
// 		}
// 		if(taskData instanceof CheckTask){
// 			this.taskListe.push(taskData);

// 		}
// 		console.log("tache ajouté: "+taskData);
// 		console.log(this.taskListe);
// 	}

// 	setDoneTask(task){
// 		if(task instanceof String){
// 			for (var i = this.taskListe.length - 1; i >= 0; i--) {
// 				if(this.taskListe[i].getTaskString() == task){
// 					this.taskListe[i].setStatus(true);
// 				}
// 			}
// 			console.log("état changé");
// 		}

// 	}

// 	printTasks(){
// 		var retour="";
// 		for (var i = this.taskListe.length - 1; i >= 0; i--) {
// 			retour=retour+this.taskListe[i].taskString+": "+this.taskListe[i].isDone+"\n";
// 		}
// 		return retour;
// 	}
// }


// var test=new TackList();
// test.addTask("débuger");
// test.addTask("dormir");
// test.addTask("manger");
// test.setDoneTask(new CheckTask("dormir"));
// console.dir(test);


let listeDesTaches = ["débuger", "dormir", "manger"];

// alert(listeDesTaches[0]);




//on va mettre le listing dans une fonction, avec un parametre d'array
let listLesElements = (monArray) =>{
	// alert('test');
	let test3 = $('#conteneurDesTaches');
	test3.html("");
	for (var i = monArray.length - 1; i >= 0; i--) {
		let newLine = document.createElement("br");
		let checkBox = document.createElement("input");
		checkBox.type="checkbox";
		//checkBox.addClass("material-switch");
		checkBox.name=monArray[i];

		test3.append(checkBox);
		test3.append(" <label for="+monArray[i]+">"+monArray[i]+"</label>");
		test3.append(newLine);

	//notre array va etre manipulé selon les checkbox
	$('div input').change(function(){
		//alert(listeDesTaches);
		let index = listeDesTaches.indexOf(this.name);
		if (index > -1) {
		  listeDesTaches.splice(index, 1);
		};
	});
		// test3.innerHTML = listeDesTaches[i];
	}
};


//dans un premier étape, on va lister tous les éléments

listLesElements(listeDesTaches);



//une fois on click sur valider, on rappelle la fonction avec l'array manipulé
$('#submit').on('click', function(){
	listLesElements(listeDesTaches);
});


// <input type="checkbox" name="cca"><span>test</span>
//console.log(listeDesTaches);
